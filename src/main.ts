import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { loadAllPlugins } from '@/plugins'
import Codemirror from "codemirror-editor-vue3";
// plugin-style
import "codemirror-editor-vue3/dist/style.css";
import '@/styles/antd.less'
import '@/styles/bpmn.less'

const app = createApp(App)
loadAllPlugins(app)
app.use(store)
app.use(router)
app.use(Codemirror)
app.mount('#app')
