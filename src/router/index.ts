import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/main/main.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/home', // 设置默认打开的页面
    children: [
      {
        path: 'home',
        name: 'home',
        component: () => import('../views/dashboard/home.vue')
      },
      {
        path: 'workflow-design',
        name: 'workflow-design',
        component: () => import('../views/workflow/workflow-design.vue')
      },
      {
        path: 'workflow-pubsish',
        name: 'workflow-pubsish',
        component: () => import('../views/workflow/workflow-pubsish.vue')
      }
    ]
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/dashboard/about.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName: "about" */ '../views/login/index.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
